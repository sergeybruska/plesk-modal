var control = document.getElementById('control-circle');
var progressValue = document.querySelector('.modal-box--circle-progress_ing');
var progressNum = document.querySelector('.modal-box--circle-progress_num');

var RADIUS = 54;
var CIRCUMFERENCE = 2 * Math.PI * RADIUS;

function progress(value) {
	var progress = value / 100;
	var dashoffset = CIRCUMFERENCE * (1 - progress);
	
  progressNum.innerHTML=value +'<sup>%</sup>';
	console.log('progress:', value + '%', '|', 'offset:', dashoffset)
	
	progressValue.style.strokeDashoffset = dashoffset;
}

control.addEventListener('input', function(event) {
	progress(event.target.valueAsNumber);
});

progressValue.style.strokeDasharray = CIRCUMFERENCE;
progress(60);